__author__ = 'Jaewon'


def fibonacci(x):
    n1 = 0
    n2 = 1
    if x == 0:
        return 0
    elif x == 1:
        return 1
    else:
        for i in range(0,x-1):
            n2 += n1  # new number is sum of n1 and n2
            n1 = n2 - n1
    return n2

print fibonacci(0)
print fibonacci(1)
print fibonacci(5)
print fibonacci(12)