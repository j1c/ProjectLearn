__author__ = 'Jaewon'

def sum_of_digits(x):
    n = 0
    for i in range(len(str(x))):
        n += x % 10
        x /= 10
    return n

print sum_of_digits(123)