__author__ = 'Jaewon'


def is_even(x):
    if x % 2 == 0:
        return 1  # Even numbers are 1
    else:
        return 0  # Odd numbers are 0