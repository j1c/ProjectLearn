"""
Observations:
1) To get at least 1 trillion, I need to travel at least 13 (9^n) and at most 40 (2^n) squares. 
2) I can only move to squares that has a number on my die (total of 9 numbers so if I have 6 unique sides, 3 numbers are excluded).
3) There are no 9s.
4) The second roll must be 3 or 5, and second to last roll must be 3 or 7. At least I lose 2 sides. At most, I lose 3.
5) Probably best to used as many empty squares as possible.
Die position:   Die numbers:
    3               1
  2 1 5           0 3 0
    4               1
    6               0
"""


import numpy as np
from operator import add, sub


class Die(object):
    def __init__(self, side_one, side_two, side_three, side_four, side_five, side_six):
        self._sides = [side_one, side_two, side_three, side_four, side_five, side_six]

    @property
    def sides(self):
        return self._sides[2]

    def roll(self, val):
        d = {'right': ((5, 3), (1, 5), (2, 1), (3, 2)),
             'left': ((3, 5), (5, 1), (1, 2), (2, 3)),
             'up': ((0, 2), (2, 4), (4, 5), (5, 0)),
             'down': ((2, 0), (4, 2), (5, 4), (0, 5))}

        self._sides[d[val][0][0]], self._sides[d[val][1][0]], self._sides[d[val][2][0]], self._sides[d[val][3][0]] = \
            self._sides[d[val][0][1]], self._sides[d[val][1][1]], self._sides[d[val][2][1]], self._sides[d[val][3][1]]

    def check(self, val):
        d = {'right': 1,
             'left': 3,
             'up': 4,
             'down': 0}

        return self._sides[d[val]]

    def print_die(self):
        print '  {}  '.format(self._sides[0])
        print '{} {} {}'.format(self._sides[1], self._sides[2], self._sides[3])
        print '  {}  '.format(self._sides[4])
        print '  {}  '.format(self._sides[5])


class Board(object):
    def __init__(self, die):
        self._grid = np.array([[1, 5, 4, 4, 6, 1, 1, 4, 1, 3, 7, 5],
                               [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                               [4, 0, 6, 4, 1, 8, 1, 4, 2, 1, 0, 3],
                               [7, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 2],
                               [1, 0, 1, 0, 6, 1, 6, 2, 0, 2, 0, 1],
                               [8, 0, 4, 0, 1, 0, 0, 8, 0, 3, 0, 5],
                               [4, 0, 2, 0, 5, 0, 0, 3, 0, 5, 0, 2],
                               [8, 0, 5, 0, 1, 1, 2, 3, 0, 4, 0, 6],
                               [6, 0, 1, 0, 0, 0, 0, 0, 0, 3, 0, 6],
                               [3, 0, 6, 3, 6, 5, 4, 3, 4, 5, 0, 1],
                               [6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
                               [2, 1, 6, 6, 4, 5, 2, 1, 1, 1, 7, 1]])
        self._traversed = np.zeros((12, 12), dtype=int)
        self._traversed[(0, 0)] = 1
        self._pos = [0, 0]
        self._tot = 1
        self._die = die

    @property
    def pos(self):
        return tuple(self._pos)

    @property
    def grid(self): #replace current position with * in grid
        self.a = self._grid.astype(str)
        self.a[self.pos] = '*'
        for i in self.a.tolist():
            print ' '.join(i)

    def move(self, val):
        direction = {'down': [1, 0], 'up': [-1, 0], 'right': [0, 1], 'left': [0, -1]}
        assert -1 not in map(add, self._pos, direction[val]), 'Moving outside grid!'
        assert self._traversed[tuple(map(add,self._pos, direction[val]))] != 1, 'Already traversed square!'
        assert self._die.check(val) == self._grid[tuple(map(add,self._pos, direction[val]))] or self._grid[tuple(map(add,self._pos, direction[val]))] == 0, "Grid and die don't match!"

        self._pos = tuple(map(add, self._pos, direction[val]))
        self._traversed[self.pos] = 1
        self._die.roll(val)
        self._tot *= self._die.sides


def traverse(board, direction):
    board.move(direction)
    print board.grid
    print board._tot
    print board._die.print_die()

if __name__ == '__main__':
    b = Board(Die(3, 2, 1, 5, 4, 6))
